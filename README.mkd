# Biseau
Build by Input Some Easy ASP from User.

Helps the exploration of formal structures by using ASP as a DSL for drawing graphs.
For a Graphical User Interface, see the [biseau-gui](https://gitlab.inria.fr/lbourneu/biseau-gui) package.


For the initial version of biseau published in the
[FCA4AI'18 workshop](https://fca4ai.hse.ru/2018) at IJCAI,
see [the archived repository biseau-initial](https://gitlab.inria.fr/lbourneu/biseau-initial)


## Principles

Now, you may want to read about:

- [visualization details](doc/user-doc.mkd#visualization-principles), that explains the theory behind the visualization itself.
- [visualization DSL](doc/user-doc.mkd#asp-to-dot), that explains which atoms produces which effect in visualization.
- [scripting](doc/user-doc.mkd#scripting), that allow you to implement new behavior for biseau.


### Export as standalone script
Currently under development.




## Installation
See [user documentation](doc/user-doc.mkd#installation) for details,
but in short, you need [clingo](https://potassco.org/clingo/),
[graphviz](http://graphviz.org/) and `make install-deps`.

Biseau and its gui are packaged, so if you already have clingo and graphviz, you should be good with:

    pip install biseau biseau-gui


## Usage
See [user documentation](doc/user-doc.mkd#basic-usage) for details.
See [`Makefile`](Makefile) for various recipes.
Running biseau can be done using the following commands:

    python -m biseau gui     # run the Graphical User Interface (if biseau-gui is installed)
    python -m biseau         # run the Command Line Interface
    python -m biseau --help  # get help for the CLI


# Notes
### Bomb attack error
To save you from some kind of attacks, Pillow may prevent you to create large images. To fix that, you can use something like:

    from PIL import Image
    Image.MAX_IMAGE_PIXELS = 20000  # or None, if you are feeling adventurous


