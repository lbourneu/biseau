set local
REM This script runs biseau's GUI, asserting that user is running it
REM  while in the same directory as this script.
REM Also, it will add the path to graphviz/dot.exe if
REM  it exist in the local PATH env copy.

REM Add path to dot
set DOTPATH="C:\Program Files (x86)\Graphviz2.38\bin"
IF EXIST DOTPATH SET PATH=%PATH%;DOTPATH

REM run biseau
cd ..
python -m biseau gui

REM you should make a copy of this script, if you want to modify it,
REM  and modify/use the copy instead.
REM Probably you will want to modify path to clingo. Use --clingo option.
