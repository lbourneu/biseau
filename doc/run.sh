# This script runs biseau's GUI, asserting that user is running it
#  while in the same directory as this script.
cd ..
python -m biseau gui

# you should make a copy of this script, if you want to modify it,
#  and modify/use the copy instead.
# Probably you will want to modify path to clingo. Use --clingo option.
