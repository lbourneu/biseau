% Generate iceberg concepts mined in an input context,
%  with support >= minsupp.
%
%
% INPUT:
%   - rel(X,Y):- object X is in relation with attribute Y.
% CONST:
%   - allow_non_concept: set it to ¬0 to get also the infinum and supremum.
%   - minsupp_obj: minimal number of objects in yielded concepts
%   - minsupp_att: minimal number of attributes in yielded concepts
% OUTPUT (one model == one concept):
%   - ext(X):- X is an object of the concept (extent)
%   - int(Y):- Y is an attribute of the concept (intent)
#const minsupp_obj=0.
#const minsupp_att=0.


% Generate the concept.
ext(X):- rel(X,_) ; rel(X,Y): int(Y).
int(Y):- rel(_,Y) ; rel(X,Y): ext(X).

% Discard any non frequent concept.
% not_enough:- {ext(_)} < minsupp_obj ; minsupp_obj > 0.
% not_enough:- {int(_)} < minsupp_att ; minsupp_att > 0.

% :- not_enough.
:- {ext(_)} > 0 ; {ext(_)} < minsupp_obj ; minsupp_obj>0.
:- {int(_)} > 0 ; {int(_)} < minsupp_att ; minsupp_att>0.

% Place support as extent
outsider(Y):- int(Y) ; rel(Z,Y) ; not ext(Z).
specint(Y):- int(Y) ; not outsider(Y).
specext(X):- X={ext(_)}.

#show.
#show ext/1.
#show int/1.
#show specext/1.
#show specint/1.
