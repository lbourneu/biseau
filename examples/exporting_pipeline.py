"""Example of pipeline export"""
import biseau as bs

def take_scripts():
    scripts = 'scripts/context.py', 'scripts/build_concepts.py', 'scripts/galois-lattice.json', 'scripts/show_galois_lattice.py'
    for name in scripts:
        yield from bs.build_scripts_from_file(name)

# get scripts and their options
scripts = tuple(take_scripts())
options = bs.export.get_pipeline_options(scripts)
# print('OPTIONS:', options)
# for script in scripts:
    # print(f"Option of script {script.name} are: {script.options}")

# setting some options to be exposed in the final CLI/GUI
options['fname'] = True
options['type'] = True
scripts[0].options_values['fname'] = 'contexts/human.cxt'

# output to outfile
OUTFILE = 'out-export.py'
with open(OUTFILE, 'w') as fd:
    fd.write(bs.standalone_export_pipeline(scripts, options))
print(OUTFILE, 'written')
