"""Basic example of API usage"""

import biseau as bs

# straightforward way : just use default options for all scripts
bs.single_image_from_filenames(
    fnames=('scripts/raw_data.lp', 'scripts/compute_score.py', 'scripts/render_interactions.json'),
    outfile='interactions.png',
    dotfile='interactions.dot',
)


# We could also load all scripts in a given dir (without garantee over script order)
scripts = list(bs.build_scripts_from_dir('scripts/'))

# Or from a single filename
scripts.extend(bs.build_scripts_from_file('myscripts/hello-world.json'))

# Now, we can choose any subset of these scripts to make a context describing a graph
final_context = bs.run(scripts[:3])
# this context (which is pure ASP) can be compiled to dot
print(final_context)
bs.compile_to_single_image(final_context, outfile='example.png')

# note that scripts are complex objects, that have options:
a_script = next(bs.build_scripts_from_file('scripts/compute_score.py'))
print(f"Script {a_script.name} have options:", a_script.options)
print("  Current values are:", a_script.options_values)
print("  Changing values… ")
a_script.options_values['multiplier'] = 3
print("  Now values are:", a_script.options_values)
