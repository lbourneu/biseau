
import os
import tempfile
import biseau
from biseau import standalone_export_pipeline
from biseau.module_loader import build_scripts_from_asp_code

ASP_LINK = 'link(a,b).'

def test_standalone_export_pipeline():
    scripts = tuple((
        *build_scripts_from_asp_code(ASP_LINK),
        *build_scripts_from_asp_code('link(X,1..3) :- link(X,_).'),
    ))
    code = standalone_export_pipeline.without_formatting(scripts)
    print('CODE:', code)
    env = {}
    module = exec(code, env)
    # print('ENV:', env)  # generetae a lot of warning because of the object printing versions that use outdated code
    assert callable(env['run_on'])
    assert callable(env['cli'])
    assert biseau.__version__ in env['__doc__']


def test_standalone_export_pipeline_with_different_script_format():
    with tempfile.NamedTemporaryFile(mode='w', delete=False, suffix='.py') as fd:
        fd.write('def run_on(context):\n    return "node(a)."\n')
        pyfile = fd.name
    scripts = tuple((
        biseau.module_loader.build_script_from_json({
            'name': 'test-python-1',
            'python': 'def run_on(context, *, nodename:str="b"):\n    return f"node({nodename})."\n',
        }),
        biseau.module_loader.build_script_from_json({
            'name': 'test-python-2',
            'python file': pyfile,
        }),
        biseau.module_loader.build_script_from_json({
            'name': 'test-python-3',
            'ASP': 'node(c).',
        }),
        *build_scripts_from_asp_code('link(X,Y) :- node(X) ; node(Y) ; X<Y.'),
    ))
    assert scripts[0].options == (('nodename', str, 'b', ''),)
    assert 'nodename' not in scripts[0].options_values
    scripts[0].options_values['nodename'] = 'bb'
    final_context = biseau.run(scripts)
    print('FINAL CONTEXT:', final_context)
    code = standalone_export_pipeline.without_formatting(scripts, options={'nodename': True})
    print('\nCODE:', code)
    env = {}
    module = exec(code, env)
    # print('ENV:', env)  # generetae a lot of warning because of the object printing versions that use outdated code
    assert callable(env['run_on'])
    assert callable(env['cli'])
    assert biseau.__version__ in env['__doc__']
    found_context = env['run_on']('', nodename='bb')
    print('FOUND CONTEXT:', found_context)
    assert found_context == final_context
    os.unlink(pyfile)
