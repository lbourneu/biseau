
import pytest
import itertools
from collections import defaultdict
from pydot import graph_from_dot_data
import biseau
from biseau import run, compile_context_to_dots
from biseau.module_loader import build_scripts_from_asp_code


ASP_LINK = 'link(a,b).'


def test_basic():
    scripts = tuple(build_scripts_from_asp_code(ASP_LINK))
    assert len(scripts) == 1
    script = next(iter(scripts))
    assert script.description == 'inline ASP code'
    assert script.name == 'inline ASP code'
    assert script.input_mode is str
    assert script.source_code == ASP_LINK
    assert script.language == 'asp'
    assert not script.options
    assert not script.options_values
    assert not script.erase_context


def test_pipeline():
    scripts = tuple((
        *build_scripts_from_asp_code(ASP_LINK),
        *build_scripts_from_asp_code('link(X,1..3) :- link(X,_).'),
    ))
    assert len(scripts) == 2
    final_dots = tuple(compile_context_to_dots(run(scripts)))
    assert len(final_dots) == 1
    final_dot = ''.join(final_dots[0])
    graphs = tuple(_graphdict_from_dot(final_dot))
    assert len(graphs) == 1
    graph = graphs[0]
    assert graph == {'a': {'b', '1', '2', '3'}}

def _graphdict_from_dot(dot:str) -> dict:
    "Convert dot data into a dict describing the graph ; yield graph"
    graphs = graph_from_dot_data(dot)
    for graph in graphs:
        dictgraph = defaultdict(set)  # source: target
        for edge in graph.get_edges():
            dictgraph[edge.get_source()].add(edge.get_destination())
        yield dict(dictgraph)


def test_python_builder():
    script = biseau.module_loader.build_script_from_json({
        'name': 'test-python',
        'python': 'def run_on(context):\n    return "a."\n',
    })
    context = script.run_on('b :- a.')
    expected = 'b :- a.\na.'
    print('CONTEXT :', context)
    print('EXPECTED:', expected)
    assert context == expected
    assert script.name == 'test-python'


def test_python_builder_with_internal_code_modifiers():
    script = biseau.module_loader.build_script_from_json({
        'name': 'test-python',
        'python': 'NAME = "test-python-from-code"\nOUTPUTS = {"a/0"}\nERASE_CONTEXT = True\ndef run_on(context):\n    return "a."\n',
    })
    context = script.run_on('b :- a.')
    expected = 'a.'
    print('CONTEXT :', context)
    print('EXPECTED:', expected)
    assert context == expected
    assert script.name == 'test-python-from-code'  # shallowed by json
    assert script.outputs() == {'a/0'}


def test_python_builder_with_modifications():
    script = biseau.module_loader.build_script_from_json({
        'name': 'test-python',
        'python': 'def run_on(context):\n    yield "a."\n',
    })
    assert script.run_on('b :- a.') == script.run_on('b :- a.')
    # change the python code, verifying that the changes are really made
    script.source_code = script.source_code.replace('a', 'b')
    assert script.run_on('a :- b.') == 'a :- b.\nb.'
    # change of language and code
    script.language = 'asp'  # no direct effect
    script.source_code = 'b.'  # this one triggers the script.run_on replacement
    assert script.run_on('a :- b.') == 'a :- b.\nb.'
    # name is conserved between switches
    assert script.name == 'test-python'


def test_python_builder_with_args_and_modifications():
    script = biseau.module_loader.build_script_from_json({
        'name': 'test-python',
        'python': 'def run_on(context, *, name:str="a"):\n    yield f"{name}."\n',
    })
    assert script.language == 'python'
    assert script.options_values == {}
    assert script.run_on('') == 'a.'
    script.options_values['name'] = 'b'
    assert script.run_on('') == 'b.'
    script.options_values['name'] = 'a'
    assert script.run_on('') == 'a.'
    assert script.options == (('name', str, 'a', ''),)
    script.source_code = 'def run_on(context, *, new_name:str="a"):\n    yield f"{new_name}."\n'
    assert script.options == (('new_name', str, 'a', ''),)
    assert script.run_on('') == 'a.'
    script.options_values['new_name'] = 'b'
    # let's try to set an unexisting option
    script.options_values['name'] = 'c'
    with pytest.raises(TypeError) as err:
        script.run_on('') == 'b.'
    err.value == "run_on() got an unexpected keyword argument 'name'"


def test_python_builder_with_complex_args():
    script = biseau.module_loader.build_script_from_json({
        'name': 'test-python',
        'python': 'def run_on(context, *, a_range:range(12, 16, 2)):\n    yield f"nb({a_range})."',
    })
    assert script.language == 'python'
    assert script.options_values == {}
    script.options_values['a_range'] = 12
    assert script.run_on('') == 'nb(12).'
    assert script.options == (('a_range', range(12, 16, 2), 12, ''),)

def test_complex_parsing():
    mod = biseau.module_loader.build_script_from_json({
        'name': 'test',
        'ASP': 'test((a,b,c)). test(test((a,b,c))). a(a(a((2,)),(2,3))).'
    })
    context = mod.run_on('')
    assert context == 'test((a,b,c)). test(test((a,b,c))). a(a(a((2,)),(2,3))).'
    # assert False, "TODO: prouver qu'ASP gère les trucs style a(a(a((2,)),(2,3)))"

def test_script_sorting():
    one = biseau.module_loader.build_script_from_json({
        'name': 'test-one',
        'ASP': 'one.',
        'inputs': ['a/1'],
        'outputs': ['b/1'],
    })
    two = biseau.module_loader.build_script_from_json({
        'name': 'test-two',
        'ASP': 'two.',
        'inputs': [],
        'outputs': ['a/1'],
    })
    tee = biseau.module_loader.build_script_from_json({
        'name': 'test-tee',
        'ASP': 'tee.',
        'inputs': ['b/1'],
        'outputs': [],
    })
    scripts = tuple(biseau.sort_scripts_per_dependancies((one, two, tee)))
    print(tuple(map(str, scripts)))
    assert scripts[0].name == 'test-two'
    assert scripts[1].name == 'test-one'
    assert scripts[2].name == 'test-tee'


def test_script_sorting_with_wildcard():
    one = biseau.module_loader.build_script_from_json({
        'name': 'test-one',
        'ASP': 'one.',
        'inputs': ['a/1'],
        'outputs': ['*/*', 'b/1'],
    })
    two = biseau.module_loader.build_script_from_json({
        'name': 'test-two',
        'ASP': 'two.',
        'inputs': [],
        'outputs': ['*/*', 'a/1'],
    })
    tee = biseau.module_loader.build_script_from_json({
        'name': 'test-tee',
        'ASP': 'tee.',
        'inputs': ['b/1'],
        'outputs': [],
    })
    scripts = tuple(biseau.sort_scripts_per_dependancies((one, two, tee)))
    print(tuple(map(str, scripts)))
    assert scripts[0].name == 'test-two'
    assert scripts[1].name == 'test-one'
    assert scripts[2].name == 'test-tee'
