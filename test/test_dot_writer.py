

from biseau import asp_to_dot


def test_basic_visual_config():
    config = asp_to_dot.visual_config_from_atoms({}, {
        'link': {('a', 'b'), ('c', 'd')},
        'color': {('a', 'red')}
    }, ' ')
    assert set(config.arcs) == {('a', 'b'), ('c', 'd')}
    assert config.properties == {'a': {'fillcolor': 'red'}}


def test_basic_visual_config_2():
    config = asp_to_dot.visual_config_from_atoms({}, {
        'link': {('a', 'b'), ('c', 'd')},
        'node': {('e',)},
    }, ' ')
    assert set(config.arcs) == {('a', 'b'), ('c', 'd')}
    assert set(config.nodes) == {'e'}


def test_complex_labelling():
    config = asp_to_dot.visual_config_from_atoms({}, {
        'link': {('a', 'b'), ('c', 'd')},
        'label': {('a', '"toto"'), ('a', '"tutu"')},
        'color': {('a','green'), ('b','green:red;0.25:blue')},
    }, ' ')
    assert set(config.arcs) == {('a', 'b'), ('c', 'd')}
    # print(dir(config))
    print(config.properties['a'])
    assert set(config.properties['a']['label'].split()) == {'toto', 'tutu'}
    assert config.properties['a']['fillcolor'] == 'green'
    assert config.properties['b']['fillcolor'] == 'green:red;0.25:blue'


def test_colors():
    config = asp_to_dot.visual_config_from_atoms({}, {
        'link': {('a', 'b'), ('c', 'd')},
        'color': {('a','green'), ('b','#00FF00'), ('c','#00FF0099'), ('d','#00FF0033')},
    }, ' ')
    assert set(config.arcs) == {('a', 'b'), ('c', 'd')}
    # print(dir(config))
    print(config.properties['a'])
    assert config.properties['a']['fillcolor'] == 'green'
    assert config.properties['b']['fillcolor'] == '#00FF00'
    assert config.properties['c']['fillcolor'] == '#00FF0099'
    assert config.properties['d']['fillcolor'] == '#00FF0033'
