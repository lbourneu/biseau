VERBOSITY=
# VERBOSITY=-v
# VERBOSITY=-vv
# VERBOSITY=-vvv

run-test:
	python -m biseau scripts/example.lp scripts/black_theme.json -o out/out.png $(VERBOSITY)
run-fca-test:
	python -m biseau -c ./configs/concept-lattice.json -o out/concept-lattice.png $(VERBOSITY)
run-einstein-test:
	python -m biseau -c ./configs/einstein-riddle.json -o out/einstein-riddle.png $(VERBOSITY)
run-config-test:
	python -m biseau -c ./configs/simple.json $(VERBOSITY)
	python -m biseau -c ./configs/concept-lattice.json -o out/concept-lattice.png $(VERBOSITY)
	xdg-open out/concept-lattice.png

run-gif-test:
	python -m biseau examples/path-to-gif.lp --dotfile "out/todel{}.dot" --outfile out/out.gif --gif-duration 1000 $(VERBOSITY)

t: test
test:
	python -m pytest biseau test -vv --ignore=venv --doctest-modules

fullrelease:
	fullrelease
install_deps:
	python -c "import configparser; c = configparser.ConfigParser(); c.read('setup.cfg'); print(c['options']['install_requires'])" | xargs pip install -U
install:
	python setup.py install

.PHONY: t test
